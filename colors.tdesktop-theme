////////////////////////////////////////////////////////////////////////
//////////////                DarkBlue Theme              //////////////
//////////////      by @Soshaw | Channel: @soshawnet      //////////////
////////////// https://gitlab.com/XiNiX/DarkBlueTheme-tg/ //////////////
////////////////////////////////////////////////////////////////////////


BLUE: #1a98ff;
BLUE_DARK: #0062b3;
WHITE: #FFFFFF;
DARK: #252526;
BLACK: #0d1011;
RED: #ff0000;
YELLOW: #ffff00;

GRAY1: #d4d7d6;
GRAY2: #9393931a;
GRAY3: #919191;

DARK1: #333333;
DARK2: #373737;
DARK3: #FFFFFF;

BLUE1: #519aba;


windowBg: DARK;
windowFg: GRAY1;
windowBgOver: DARK1;
windowBgRipple: BLUE_DARK;
windowFgOver: GRAY1;
windowSubTextFg: WHITE;
windowSubTextFgOver: GRAY3;
windowBoldFg: GRAY1; 
windowBoldFgOver: GRAY1;
windowBgActive: BLUE;
windowFgActive: WHITE;
windowActiveTextFg: BLUE;
windowShadowFg: GRAY1;
windowShadowFgFallback: DARK1;
shadowFg: #d4d7d618;

slideFadeOutBg: #d4d7d63c;
slideFadeOutShadowFg: GRAY1;

imageBg: GRAY1;
imageBgTransparent: DARK;

activeButtonBg: BLUE;
activeButtonBgOver: BLUE;
activeButtonBgRipple: BLUE_DARK;
activeButtonFg: WHITE;
activeButtonFgOver: WHITE;
activeButtonSecondaryFg: #cceeff;
activeButtonSecondaryFgOver: #cceeff;
activeLineFg: BLUE;
activeLineFgError: #e48383;

lightButtonBg: DARK;
lightButtonBgOver: BLUE;
lightButtonBgRipple: BLUE_DARK;
lightButtonFg: WHITE;
lightButtonFgOver: WHITE;

attentionButtonFg: #d14e4e;
attentionButtonFgOver: #d14e4e;
attentionButtonBgOver: DARK2;
attentionButtonBgRipple: #f4c3c2;

outlineButtonBg: DARK;
outlineButtonBgOver: DARK2;
outlineButtonOutlineFg: BLUE;
outlineButtonBgRipple: BLUE_DARK;

menuBg: DARK;
menuBgOver: DARK1;
menuBgRipple: BLUE_DARK;
menuIconFg: BLUE;
menuIconFgOver: BLUE;
menuSubmenuArrowFg: DARK2;
menuFgDisabled: #cccccc;
menuSeparatorFg: DARK1;

scrollBarBg: BLUE;
scrollBarBgOver: BLUE_DARK;
scrollBg: GRAY2;
scrollBgOver: GRAY2;

smallCloseIconFg: BLUE;
smallCloseIconFgOver: BLUE_DARK;

radialFg: WHITE;
radialBg: #d4d7d656;

placeholderFg: WHITE;
placeholderFgActive: #aaaaaa;

inputBorderFg: #e0e0e0;
filterInputBorderFg: BLUE;

checkboxFg: WHITE;

sliderBgInactive: BLUE;
sliderBgActive: #e1eaef;

tooltipBg: #eef2f5;
tooltipFg: #5d6c80;
tooltipBorderFg: #c9d1db;

titleBg: DARK1;
titleShadow: #d4d7d603;
titleButtonFg: #ababab;
titleButtonBgOver: #0d1011;
titleButtonFgOver: #9a9a9a;
titleButtonCloseBgOver: #e81123;
titleButtonCloseFgOver: WHITE;
titleFgActive: GRAY1;
titleFg: WHITE;

trayCounterBg: #f23c34;
trayCounterBgMute: #888888;
trayCounterFg: WHITE;
trayCounterBgMacInvert: WHITE;
trayCounterFgMacInvert: #ffffff01;

layerBg: #3333337f;

cancelIconFg: BLUE;
cancelIconFgOver: BLUE;

boxBg: DARK;
boxTextFg: GRAY1;
boxTextFgGood: #4ab44a;
boxTextFgError: #d84d4d;
boxTitleFg: WHITE;
boxSearchBg: boxBg;
boxSearchCancelIconFg: BLUE;
boxSearchCancelIconFgOver: BLUE;
boxTitleAdditionalFg: #808080;
boxTitleCloseFg: BLUE;
boxTitleCloseFgOver: BLUE;

membersAboutLimitFg: GRAY3;

contactsBg: DARK;
contactsBgOver: DARK1;
contactsNameFg: boxTextFg;
contactsStatusFg: WHITE;
contactsStatusFgOver: GRAY3;
contactsStatusFgOnline: WHITE;

photoCropFadeBg: layerBg;
photoCropPointFg: #2525267f;

introBg: DARK;
introTitleFg: GRAY1;
introDescriptionFg: WHITE;
introErrorFg: WHITE;
introCoverTopBg: DARK1;
introCoverBottomBg: DARK;
introCoverIconsFg: DARK;
introCoverPlaneTrace: #3a3a3b;
introCoverPlaneInner: #4d4d4d;
introCoverPlaneOuter: #424243;
introCoverPlaneTop: DARK;

dialogsMenuIconFg: BLUE;
dialogsMenuIconFgOver: BLUE;
dialogsBg: DARK;
dialogsNameFg: BLUE;
dialogsChatIconFg: BLUE;
dialogsDateFg: WHITE;
dialogsTextFg: GRAY1;
dialogsTextFgService: GRAY1;
dialogsDraftFg: #dd4b39;
dialogsVerifiedIconBg: BLUE1;
dialogsVerifiedIconFg: WHITE;
dialogsSendingIconFg: #c1c1c1;
dialogsSentIconFg: WHITE;
dialogsUnreadBg: BLUE;
dialogsUnreadBgMuted: BLACK;
dialogsUnreadFg: DARK1;
dialogsBgOver: DARK1;
dialogsNameFgOver: BLUE_DARK;
dialogsChatIconFgOver: BLUE_DARK;
dialogsDateFgOver: GRAY3;
dialogsTextFgOver: GRAY3;
dialogsTextFgServiceOver: GRAY3;
dialogsDraftFgOver: #dd4b39;
dialogsVerifiedIconBgOver: BLUE1;
dialogsVerifiedIconFgOver: WHITE;
dialogsSendingIconFgOver: #c1c1c1;
dialogsSentIconFgOver: WHITE;
dialogsUnreadBgOver: BLUE;
dialogsUnreadBgMutedOver: BLACK;
dialogsUnreadFgOver: DARK1;
dialogsBgActive: #0d1011;
dialogsNameFgActive: WHITE;
dialogsChatIconFgActive: WHITE;
dialogsDateFgActive: WHITE;
dialogsTextFgActive: WHITE;
dialogsTextFgServiceActive: WHITE;
dialogsDraftFgActive: WHITE;
dialogsVerifiedIconBgActive: BLUE1;
dialogsVerifiedIconFgActive: WHITE;
dialogsSendingIconFgActive: #25252699;
dialogsSentIconFgActive: WHITE;
dialogsUnreadBgActive: WHITE;
dialogsUnreadBgMutedActive: WHITE;
dialogsUnreadFgActive: #0d1011;
dialogsForwardBg: #0d1011;
dialogsForwardFg: WHITE;

searchedBarBg: DARK1;
searchedBarBorder: #d4d7d618;
searchedBarFg: GRAY3;

topBarBg: DARK;

emojiPanBg: DARK1;
emojiPanCategories: DARK;
emojiPanHeaderFg: WHITE;
emojiPanHeaderBg: DARK1;

stickerPanDeleteBg: BLUE;
stickerPanDeleteFg: WHITE;
stickerPreviewBg: #252526b0;

historyTextInFg: GRAY1;
historyTextOutFg: GRAY1;
historyCaptionInFg: GRAY1;
historyCaptionOutFg: GRAY1;
historyFileNameInFg: GRAY1;
historyFileNameOutFg: GRAY1;
historyOutIconFg: WHITE;
historyOutIconFgSelected: #d4d4d4;
historyIconFgInverted: WHITE;
historySendingOutIconFg: #a0adb5;
historySendingInIconFg: #a0adb5;
historySendingInvertedIconFg: #252526c8;
historySystemBg: #89a0b47f;
historySystemBgSelected: #bbc8d4a2;
historySystemFg: WHITE;
historyUnreadBarBg: #99999980;
historyUnreadBarBorder: #d4d7d618;
historyUnreadBarFg: WHITE;
historyForwardChooseBg: BLUE;
historyForwardChooseFg: WHITE;
historyPeer1NameFg: #e5c453;
historyPeer1UserpicBg: #e5c453;
historyPeer2NameFg: #e57255;
historyPeer2UserpicBg: #e57255;
historyPeer3NameFg: #e95065;
historyPeer3UserpicBg: #e95065;
historyPeer4NameFg: #52d273;
historyPeer4UserpicBg: #52d273;
historyPeer5NameFg: #46bddf;
historyPeer5UserpicBg: #46bddf;
historyPeer6NameFg: #FF1090;
historyPeer6UserpicBg: #FF1090;
historyPeer7NameFg: #008040;
historyPeer7UserpicBg: #008040;
historyPeer8NameFg: #FF4040;
historyPeer8UserpicBg: #FF4040;
historyPeerUserpicFg: WHITE;
historyScrollBarBg: BLUE;
historyScrollBarBgOver: BLUE_DARK;
historyScrollBg: GRAY2;
historyScrollBgOver: GRAY2;

msgInBg: DARK;
msgInBgSelected: BLUE_DARK;
msgOutBg: DARK2;
msgOutBgSelected: BLUE_DARK;
msgSelectOverlay: #a0acb64c;
msgStickerOverlay: #a0acb67f;
msgInServiceFg: BLUE;
msgInServiceFgSelected: WHITE;
msgOutServiceFg: BLUE;
msgOutServiceFgSelected: WHITE;
msgInShadow: #748ea229;
msgInShadowSelected: #548dbb29;
msgOutShadow: #37373740;
msgOutShadowSelected: #37a78e40;
msgInDateFg: #a0acb6;
msgInDateFgSelected: #a0acb6;
msgOutDateFg: #a0acb6;
msgOutDateFgSelected: #a0acb6;
msgServiceFg: WHITE;
msgServiceBg: BLUE;
msgServiceBgSelected: #8ca0b3a2;
msgInReplyBarColor: BLUE;
msgInReplyBarSelColor: BLUE;
msgOutReplyBarColor: BLUE;
msgOutReplyBarSelColor: BLUE;
msgImgReplyBarColor: WHITE;
msgInMonoFg: #507a9c;
msgOutMonoFg: #507a9c;
msgDateImgFg: GRAY1;
msgDateImgBg: #333333B3;							
msgDateImgBgOver: #333333D4;						
msgDateImgBgSelected: #33333387;
msgFileThumbLinkInFg: WHITE;
msgFileThumbLinkInFgSelected: WHITE;
msgFileThumbLinkOutFg: WHITE;
msgFileThumbLinkOutFgSelected: WHITE;
msgFileInBg: BLUE;
msgFileInBgOver: BLUE_DARK;
msgFileInBgSelected: GRAY1;
msgFileOutBg: BLUE;
msgFileOutBgOver: BLUE_DARK;
msgFileOutBgSelected: GRAY1;
msgFile1Bg: #72b1df;
msgFile1BgDark: #5c9ece;
msgFile1BgOver: #5294c4;
msgFile1BgSelected: #5099d0;
msgFile2Bg: #61b96e;
msgFile2BgDark: #4da859;
msgFile2BgOver: #44a050;
msgFile2BgSelected: #46a07e;
msgFile3Bg: #e47272;
msgFile3BgDark: #cd5b5e;
msgFile3BgOver: #c35154;
msgFile3BgSelected: #9f6a82;
msgFile4Bg: #efc274;
msgFile4BgDark: #e6a561;
msgFile4BgOver: #dc9c5a;
msgFile4BgSelected: #b19d84;
msgWaveformInActive: WHITE;
msgWaveformInActiveSelected: BLACK;
msgWaveformInInactive: BLACK;
msgWaveformInInactiveSelected: GRAY1;
msgWaveformOutActive: WHITE;
msgWaveformOutActiveSelected: BLACK;
msgWaveformOutInactive: BLACK;
msgWaveformOutInactiveSelected: GRAY1;
msgBotKbOverBgAdd: BLUE;
msgBotKbIconFg: WHITE;
msgBotKbRippleBg: BLUE_DARK;

mediaInFg: #a0acb6;
mediaInFgSelected: #a0acb6;
mediaOutFg: #a0acb6;
mediaOutFgSelected: #a0acb6;

youtubePlayIconBg: RED;
youtubePlayIconFg: WHITE;

videoPlayIconBg: #2525267f;
videoPlayIconFg: GRAY1;

toastBg: #000000b2;
toastFg: WHITE;

reportSpamBg: DARK1;
reportSpamFg: GRAY1;

historyToDownShadow: #d4d7d640;
historyComposeAreaBg: DARK;
historyComposeAreaFg: GRAY1;
historyComposeAreaFgService: #a0acb6;
historyComposeIconFg: BLUE;
historyComposeIconFgOver: WHITE;
historySendIconFg: BLUE;
historySendIconFgOver: WHITE;
historyPinnedBg: DARK1;
historyReplyBg: DARK;
historyReplyCancelFg: BLUE;
historyReplyCancelFgOver: WHITE;
historyComposeButtonBg: DARK;
historyComposeButtonBgOver: DARK1;
historyComposeButtonBgRipple: BLUE_DARK;

overviewCheckBg: #d4d7d640;
overviewCheckFg: DARK;
overviewCheckFgActive: DARK;
overviewPhotoSelectOverlay: #40ace333;

profileStatusFgOver: #7c99b2;

notificationsBoxMonitorFg: GRAY1;
notificationsBoxScreenBg: WHITE;
notificationSampleUserpicFg: BLUE;
notificationSampleCloseFg: #d7d7d7;
notificationSampleTextFg: #d7d7d7;
notificationSampleNameFg: #939393;

mainMenuBg: DARK;
mainMenuCoverBg: #0d1011;
mainMenuCoverFg: WHITE;

mediaPlayerBg: DARK;
mediaPlayerActiveFg: GRAY1;
mediaPlayerInactiveFg: BLACK;
mediaPlayerDisabledFg: DARK;
mediaviewFileBg: DARK;
mediaviewFileNameFg: GRAY1;
mediaviewFileSizeFg: GRAY1;
mediaviewFileRedCornerFg: #d55959;
mediaviewFileYellowCornerFg: #e8a659;
mediaviewFileGreenCornerFg: #49a957;
mediaviewFileBlueCornerFg: #599dcf;
mediaviewFileExtFg: WHITE;
mediaviewMenuBg: #383838; 
mediaviewMenuBgOver: #505050;
mediaviewMenuBgRipple: BLUE_DARK;
mediaviewMenuFg: WHITE;
mediaviewBg: #252526eb;
mediaviewVideoBg: GRAY1;
mediaviewControlBg: #2525263c;
mediaviewControlFg: WHITE;
mediaviewCaptionBg: #11111180;
mediaviewCaptionFg: WHITE;
mediaviewTextLinkFg: #91d9ff;
mediaviewSaveMsgBg: #000000b2;
mediaviewSaveMsgFg: WHITE;
mediaviewPlaybackActive: BLUE;
mediaviewPlaybackInactive: WHITE;
mediaviewPlaybackActiveOver: BLUE_DARK;
mediaviewPlaybackInactiveOver: WHITE;
mediaviewPlaybackProgressFg: #ffffffc7;
mediaviewPlaybackIconFg: BLUE;
mediaviewPlaybackIconFgOver: BLUE_DARK;
mediaviewTransparentBg: DARK;
mediaviewTransparentFg: #cccccc;

notificationBg: DARK;
